<?php

	class Site
	{
	    public static function checkout($site)
	    {
                
	        $git_folder = '/srv/www/' . $site . '/' . $site . '.git/';
	        if ($site == 'example.com')
	        {
	            shell_exec('cd ' . $git_folder . ' && GIT_WORK_TREE=/srv/www/example.com/public_html/wp-content/themes/html5-reset git checkout -f');
	        }
	        elseif ($site == 'example2.com')
	        {
	            shell_exec('cd ' . $git_folder . ' && GIT_WORK_TREE=/srv/www/example2.com/public_html git checkout -f');
	        }
	        elseif ($site == 'example3.com')
	        {
	            shell_exec('cd ' . $git_folder . ' && GIT_WORK_TREE=/srv/www/example3.com/public_html/wp-content/themes/html5-reset git checkout -f');
	        }
	    }
	}

	if ($_REQUEST['key'] == 'TsbWK7eZXGkxipXt6C' && isset($_REQUEST['payload']))
	{
	    $payload = json_decode($_REQUEST['payload']);
	    $branch = $payload->commits[0]->branch;
	    $site_name = $payload->repository->slug;
	    if (!isset($branch) || !isset($site_name))
	    {
	        exit();
	    }
	    elseif ($branch == 'master')
	    {
	        $git_dir = '/srv/www/' . $site_name . '/' . $site_name . '.git/';
	        $output = shell_exec('cd ' . $git_dir . ' && git reset --hard HEAD && git pull origin master:master');
	        $status = shell_exec('cd ' . $git_dir . ' && git rev-parse --short HEAD');

	        //Log the request and result
	        file_put_contents('webhook.log', print_r($payload, TRUE) . "\n$output\nCurrent hash is $status\n################################################\n", FILE_APPEND);

	        //Put code into production
	        Site::checkout($site_name);
	    }
	}